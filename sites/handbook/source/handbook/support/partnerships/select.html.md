---
layout: handbook-page-toc
title: Select Partners
description: "Support specific information for select partners"
---

Select partners are by invitation only and are reserved for partners that make
a greater investment in GitLab expertise, develop services practices around
GitLab and are expected to drive greater GitLab product recurring revenues.

## Contacting Support

Select Partners contact us via the [support portal](https://support.gitlab.com).
To help them route properly, they use
[this specialized form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000837100).

This should open the ticket in the Zendesk Select Partner form in zendesk. From
here, GitLab Support talks directly to the Partner, not their customer. The
Support Plan will be assumed at Ultimate level.

**Note**: Never associate a customer to an Select Partner's organization, or
vice-versa!

## File uploads

When Select Partners needs to send support files, we have 2 current methods
available to accomodate this:

* Standard ticket uploads (20MB max)
* [Support Uploader](https://about.gitlab.com/support/providing-large-files.html#support-uploader)

