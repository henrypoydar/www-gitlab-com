---
layout: handbook-page-toc
title: "Enterprise Applications Architecture"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## High Level

### Systems Roadmap

- [Roadmap of Software](/handbook/business-ops/roadmap)

### Tech Stack/Software Applications

- [List of software applications and purpose](/handbook/business-ops/tech-stack/)
- [Tech Stack High Level](/handbook/business-ops/tech-stack-applications/)

### Baseline Entitlements

- [Baseline Entitlement High Level](/handbook/business-ops/employee-enablement/onboarding-access-requests/access-requests/baseline-entitlements/)

## Enterprise Application Ecosystem

### Landscape diagram
<div style="width: 100%; height: 640px; margin-top: 10px; margin-bottom: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width: 100%; height: 100%" src="https://lucid.app/documents/embeddedchart/ec660d06-2ce7-467e-b1bd-3661509477ec" id="jAXZ9RVgDamg"></iframe></div>

#### Landscape diagram runthrough video
<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/5VOSkx_N-w8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


### Finance Systems

- [Finance Architecture High Level](/handbook/business-ops/enterprise-applications/architecture/finance)

### Lead to Fulfillment

- [Business Fulfillment Documentation RoundUp](/handbook/business-ops/enterprise-applications/portal/)
- [Trade Compliance Operations](/handbook/business-ops/trade-compliance)
- [WIP: lead to fulfillment flow through systems and processes](https://app.lucidchart.com/documents/view/fe61ff48-c0e3-4f40-b2de-4023d48101d9)
- [System integrations and related emails](https://docs.google.com/spreadsheets/d/1j3xE6pQLfsKMri14LDcrnxbWbTwqz4Tpv9kI8UIHYCE/edit#gid=1849578778)
