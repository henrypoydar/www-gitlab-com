---
layout: handbook-page-toc
title: "Metrics Collection & Analysis"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Tools

- [KeyHole.co](http://keyhole.co/): Twitter Impressions/Engagements 
- [DE-Bot](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/de-bot):  Internal tool for issue triage and team workflow automation
- [DE-Dashboard](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/de-dashboard/): Internal tool for data collection and reporting.
- [Data Studio](https://datastudio.google.com/u/5/reporting/b1407bd2-87c6-48de-aed8-b739a5947df1/page/YsgmB): Wholistic view of all metrics collected for the Evangelist Program.
- [Requests Impression Tracker Sheet](https://docs.google.com/spreadsheets/d/10E_TagnV6xgjHorWPTpMnO1Qk33lPR9HkGHOJfa0ENM/edit#gid=1283634798)

## Metrics Collections    

We collect Twitter impressions and Youtube video views using KeyHole.co and the Youtube API respectively, which are then fed into Sisense and other internal reporting platforms.

###KeyHole

Our Keyhole plan cannot access the Twitter API. Therefore we have built a process which involves manual data creation every Monday before noon UTC. The generated JSON file is uploaded into the [DE Dashboard project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/de-dashboard). A script run by the data team imports the Twitter impressions data into Sisense.

This process involves these steps:
  - Login to KeyHole using assigned account or one available in 1Password
  - In the `Social Media Analytics` section, click though the accounts you want to obtain data from
  - Select the date range in the top right corner and copy or download the required data.
  - Access the DE-Dashboard project and provided the impressions data obtained into the `content/metrics/data.json` as necessary.

### KeyHole access by other teams

Access to the data on keyhole by other team members outside of the Developer Evagelism team is possible either for tracking other Social Media accounts as account limits permit or to obtain data, by accessing the KeyHole platform using the credentials available in 1Password. If you are adding a new account for tracking, please create an issue in the [Corporate Markeing issue tracker](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues) and tag `@gitlab-com/marketing/corporate_marketing/developer-evangelism` along with the `dev-evangelism` label for visibility.

## DE Dashboard

The [DE-Dashboard](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/de-dashboard/) is a GitLab Pages project, which serves as a central point for all data collection. It serves the following purposes:
  - [Retrieve list of DE Youtube videos](https://gitlab.com/gitlab-com/marketing/corporate_marketing/developer-evangelism/code/de-dashboard/-/blob/master/youtube.rb) to Google Sheet for tracking
  - Analyze Developer Evangelism issue creation and closure in the Corporate marketing issue tracker
  - Host the data endpoint for Social media metrics
  - Computation and report of [team request budgets](/handbook/marketing/community-relations/developer-evangelism/#scoring-requests). 
