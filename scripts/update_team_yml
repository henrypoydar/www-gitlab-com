#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

# Updates the data/team.yml file if there have been any changes to the
# team members directory.
# This is safe to run on all pushes to master, since it will not trigger itself.

TEAM_YAML=data/team.yml
BOT_USER="$TEAM_YML_UPDATE_BOT_USER"
GITLAB_TOKEN="$TEAM_YML_UPDATE_BOT_TOKEN"
ORIGIN="https://$BOT_USER:$GITLAB_TOKEN@gitlab.com/gitlab-com/www-gitlab-com.git"
API_PROJECT_BASE="https://gitlab.com/api/v4/projects/gitlab-com%2Fwww-gitlab-com"
AUTOMATION_LABEL="automation:team_members"
BRANCH="$(date '+update-team-yml-%s')"

function json_parse {
  ruby -rjson -e "puts JSON.parse(STDIN.read)$1"
}

function gitlab_api {
  curl --fail --location \
    --silent \
    --header "Authorization: Bearer $GITLAB_TOKEN" \
    --header "Accept: application/json" "$@"
}

if [ "$CI_COMMIT_REF_NAME" != "$CI_DEFAULT_BRANCH" ]; then
  echo "This script should only be run on the $CI_DEFAULT_BRANCH"
  exit 1
fi

echo "Checking whether unmerged MRs are pending"

open_mrs=$(gitlab_api "$API_PROJECT_BASE/merge_requests?labels=$AUTOMATION_LABEL&state=opened" | json_parse '.size')
if [ "$open_mrs" != "0" ]; then
  echo "A team update MR is still pending."
  exit 0
fi

echo "No MRs pending... Checking $TEAM_YAML for team member changes"

bundle exec rake build:team_yml build:verify_team

if git diff --exit-code --quiet "$TEAM_YAML"; then
  echo "$TEAM_YAML is up-to-date"
else
  echo "Updating $TEAM_YAML with latest team member changes"

  git config --global user.email "$BOT_USER@gitlab.com"
  git config --global user.name "GitLab Bot"

  git checkout -b "$BRANCH"
  git add "$TEAM_YAML"
  git commit -m "chore: Update $TEAM_YAML"

  git push "$ORIGIN" "HEAD:$BRANCH"

  echo "Creating MR for $BRANCH"
  # Create an MR and assign to @leipert & @lienvdsteen
  web_url=$(gitlab_api --request POST \
             --data "source_branch=$BRANCH" \
             --data "target_branch=$CI_DEFAULT_BRANCH" \
             --data "title=Automation: Update $TEAM_YAML" \
             --data "description=Update $TEAM_YAML with latest team_member updates. (cc @leipert @alexkalderimis)" \
             --data "assignee_ids[]=181229" \
             --data "assignee_ids[]=482964" \
             --data "labels=$AUTOMATION_LABEL" \
             --data "remove_source_branch=true" \
             "$API_PROJECT_BASE/merge_requests" \
             | json_parse "['web_url']")

  echo "Successfully created $web_url"

fi
